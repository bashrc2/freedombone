#  _____               _           _
# |   __|___ ___ ___ _| |___ _____| |_ ___ ___ ___
# |   __|  _| -_| -_| . | . |     | . | . |   | -_|
# |__|  |_| |___|___|___|___|_|_|_|___|___|_|_|___|
#
#                              Freedom in the Cloud
#
# Dockerfile for Gitlab Runner. Save Electricity and Time by saving build time
#
# License
# =======
#
# Copyright (C) 2019 Liam Hurwitz <liam@contra-bit.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Set the base image to debian

FROM debian:stretch

# File Author / Maintainer
MAINTAINER Liam Hurwitz

################## BEGIN INSTALLATION ######################

# Update the repository sources list
RUN apt-get update && \
      apt-get -y install sudo

# TODO: How to sudo without tty or askpass
RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo
##
## Install Essental Build Tools
##
RUN apt-get install git build-essential dialog openssh-client -y


##
## Install Freedombone Tools
##
RUN git clone https://gitlab.informatik.uni-bremen.de/turingcomplete/freedombone.git
RUN cd freedombone &&\
        git checkout stretch &&\
        make install

##
## Install Packages for building images
##
RUN echo "Reached Build \nInstalling Packages" &&\
        sudo freedombone-image --setup debian

# Export Terminfo so tput can be executed
RUN export TERMINFO=/usr/lib/terminfo

USER root
CMD /bin/bash
