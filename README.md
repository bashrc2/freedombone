<img src="https://code.freedombone.net/bashrc/freedombone/raw/buster/img/fbone_admin_screen.png?raw=true" width="60%"/>

Once imagined as a space of liberation, the internet today is a rabble of corporate monopolies, oppressive governments and hate mobs. Much of its empowering potential has faded or been trampled. But perhaps there is a way forward.

Freedombone is a home server system which enables you to run your own internet services, individually or as a household. It includes all of the things you'd expect such as email, chat, VoIP, wikis, blogs, social networks, and more. You can run Freedombone on an old laptop or single board computer. You can also run it on an onion address. Reclaim the internet, one server at a time.

XMPP MUC: **support@chat.freedombone.net**

Matrix room: **#fbone:matrix.freedombone.net**

See [the website](https://freedombone.net) for installation instructions and other information. It's also available via a Tor browser on http://dcxf7xthmh7dusiqgtj4sswksiifqhf4yloggpe4ucyo4zfapnbj4zyd.onion

<img src="https://code.freedombone.net/bashrc/freedombone/raw/buster/img/fbone_apps.jpg?raw=true" width="60%"/>
